package com.torres.test.docker.resources;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.torres.test.docker.entity.Hello;

@RestController
@RequestMapping("/")
public class HelloResource {
	
	@GetMapping
	public ResponseEntity<Hello> show() {
		Hello hello = new Hello("Hello Docker!!");
		return ResponseEntity.ok().body(hello);
	}

}
